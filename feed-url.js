module.exports = {
    talksport:'http://talksport.com/rss/sports-news/football/feed',
    bbc:'http://feeds.bbci.co.uk/sport/football/rss.xml?edition=uk',
    espn: 'http://www.espnfc.com/rss',
    dailymail: 'http://www.dailymail.co.uk/sport/football/index.rss',
    eyeTransfer: 'http://www.eyefootball.com/rss_news_transfers.xml',
    eyefootball: 'http://www.eyefootball.com/rss_news_transfers.xml'
};