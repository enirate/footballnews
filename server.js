const feedparser = require('feedparser-promised');
const express = require('express');
const urls = require('./feed-url');
const path = require('path');
const cors = require('cors');

const app = express();

// router instance
const api = express.Router()

var corsOption = {
    origin: 'https://quick-football.firebaseapp.com',
    optionsSuccessStatus: 200
}

// api endpoints
api.get('/all-feeds', cors(corsOption), (req, res) =>{
    let babaArray = [];
    // create a global empty array within scope
    // create several feedparser call
    // push them into local array
    // if local array is greater than 7
    // slice to 4 using 
 feedparser.parse(urls.bbc)
    .then((items)=>{
        let finalSend = [];
        let allTitleA = [];
        items.forEach(item => allTitleA.push({'title':item.title, 'link':item.link}));
        if(allTitleA.length > 7)
        {
            let prepBbc = allTitleA.slice(0,5);
            babaArray = babaArray.concat(prepBbc)
        }
        else
        {
            babaArray = babaArray.concat(allTitleA);
        }
        // res.send(allTitle); 
    })
    .then(() => {
        feedparser.parse(urls.dailymail)
        .then((items)=>{
        let allTitleB = [];
        items.forEach(item => allTitleB.push({'title':item.title, 'link':item.link}));
        if(allTitleB.length > 7 )
        {
            let prepMail = allTitleB.slice(0,5);
            babaArray = babaArray.concat(prepMail);
        }
        else
        {
            babaArray = babaArray.concat(allTitleB)
        }
        // res.send(allTitle); 
        }) 
    })
    .then(() => {
        feedparser.parse(urls.espn)
        .then((items)=>{
            let allTitle = [];
            items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
            if(allTitle.length > 9 )
            {
                let prepEspn = allTitle.slice(0,7);
                babaArray = babaArray.concat(prepEspn);
            }
            else
            {
                babaArray = babaArray.concat(allTitle)
            }
            res.send(babaArray);
        }) 
    })
    .catch(err => {
        console.log('err: ', err);
        res.send({'message':'Big error'})
    })
});

api.get('/bbc', cors(corsOption), (req, res) => {
    feedparser.parse(urls.bbc)
    .then((items)=>{
        let allTitle = [];
        items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
        res.send(allTitle);
    })
    .catch(err => console.log('err: ', err))
});

api.get('/dailymail', cors(corsOption), (req, res) => {
    feedparser.parse(urls.dailymail)
    .then((items)=>{
        let allTitle = [];
        items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
        res.send(allTitle);
    })
    .catch(err => console.log('err: ', err))
});

api.get('/espn', cors(corsOption), (req, res) => {
    feedparser.parse(urls.espn)
    .then((items)=>{
        let allTitle = [];
        items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
        res.send(allTitle);
    })
    .catch(err => console.log('err: ', err))
});

api.get('/eyefootball', cors(corsOption), (req, res) => {
    feedparser.parse(urls.eyefootball)
    .then((items)=>{
        let allTitle = [];
        items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
        res.send(allTitle);
    })
    .catch(err => console.log('err: ', err))
});

api.get('/transfer', cors(corsOption), (req, res) => {
    feedparser.parse(urls.eyeTransfer)
    .then((items)=>{
        let allTitle = [];
        items.forEach(item => allTitle.push({'title':item.title, 'link':item.link}));
        res.send(allTitle);
    })
    .catch(err => console.log('err: ', err))
});


// loading middleware
app.use('/api', api);


const port = process.env.PORT || 9040;

// test point
app.get('/startup', (req, res) =>{
    res.status(200).send('jalo');
})

// handle main routes
app.use(express.static(__dirname + '/dist'));

// handle sub-routes
app.get('/*', (req, res) =>{
    res.sendFile(path.join(__dirname + '/dist/index.html'));
})

app.listen(port, () => {
    console.log('Shear fire on PORT', port);
});